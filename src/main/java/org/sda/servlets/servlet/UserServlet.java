package org.sda.servlets.servlet;


import org.sda.servlets.repository.UserRepository;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Serwlet który pobiera liste uzytkownikow z bazy danych i przekirowuje na strone z tabelka
 */
@WebServlet(value = "/users")
public class UserServlet extends HttpServlet {

    private UserRepository userRepository;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        ApplicationContext context =
                WebApplicationContextUtils.getRequiredWebApplicationContext(
                        this.getServletContext());
        userRepository = context.getBean(UserRepository.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        request.setAttribute("usersList",userRepository.findAll()); //ustawiamy atrybut w request - bedzie dostepny na stronie na ktora przekirowujemy żądanie
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/pages/userTable.jsp"); // mówumy gdzie chcemy przekirować żądanie
        requestDispatcher.forward(request, response);

    }
}
