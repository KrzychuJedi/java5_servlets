package org.sda.servlets.controller;

import org.sda.servlets.domain.User;
import org.sda.servlets.repository.UserRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

// Springowy odpowiednik serwletu

@Controller
public class UserController {

//    Dzięki adnotacji Resource spring sam binduje odpowiedni obiekt
    @Resource
    private UserRepository userRepository;

//    uzywajac adnotacji RequestMapping określamy m.in adres na jaki trzeba wysłać żądanie i typ metody
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String printHello(ModelMap model) {
//        Zmienne zapisujemy do modelu, zamiast do requestów
        model.addAttribute("message", "Hello Spring MVC Framework!");
        return "hello"; // zwracamy nazwę strony którą wyświetlimy użytkownikowi
    }

    @RequestMapping(value = "/registerPage", method = RequestMethod.GET)
    public String registerPage(ModelMap model) {
        return "registerUser";
    }

//    @ModelAttribute pozwala na utomatyczne uzupełnienie obiektu za podstawie nazw parametrów. Zastepuje request.getParameter(''). Nazwy pół w formatce musza być takie same jak w obiekcie
//    @Validated zapewnia że obiekt przejdzie walidację, m.in. na podstawie adnotacji nad polami
//    BindingResult potrzebujemy aby zapisać wyniki walidacji
//    @RequestParam("password") zmienne których nie ma w obiekcie możemy wyciągnąć uzywajac RequestParam
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String register(@Validated @ModelAttribute User user, BindingResult bindingResult,
                           @RequestParam("password") String passwordParam, ModelMap model) {
        if (bindingResult.hasErrors()) {
//            bindingResult.getAllErrors();
            return "error";
        }
        userRepository.save(user, passwordParam);

        model.put("usersList", userRepository.findAll());
        return "userTable";
    }

}
