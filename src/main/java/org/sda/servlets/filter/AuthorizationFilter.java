package org.sda.servlets.filter;

import org.sda.servlets.domain.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Filtr który wykonuje operacje na każdym żądaniu.
 * Będzie sprawdzał czy osoba korzystajaca z aplikacji jest zalogowana
 */
//@WebFilter("/*")
public class AuthorizationFilter implements Filter {

    private List<String> whiteList = new ArrayList<>();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
//        Lista adresów dla których nie sprawdzamy czy uzytkownik jest zalogowany
        whiteList.add("/pages/login.jsp");
        whiteList.add("/login");
        whiteList.add("/pages/registerUser.jsp");
        whiteList.add("/register");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession();

//        jesli adres na który jest wysłane zadanie jest na liscie
        if (whiteList.contains(req.getRequestURI())) {
//            przetwarzamy żadanie dalej
            chain.doFilter(request, response);
        } else {
//            albo sprawdzamy czy mamy zalogowanego uzytkownika
            User user = (User) session.getAttribute("loggedInUser");
            if (user != null) {
//                jesli tak idzemy dalej
                chain.doFilter(request, response);
            } else {
//                w innym wypadku odsylamy usera do strony logowania
                resp.sendRedirect("/pages/login.jsp");
            }
        }
    }

    @Override
    public void destroy() {

    }
}
